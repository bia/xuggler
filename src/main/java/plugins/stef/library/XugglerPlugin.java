package plugins.stef.library;

import com.xuggle.ferry.Ferry;

import icy.plugin.abstract_.Plugin;
import icy.plugin.interface_.PluginLibrary;

/**
 * Xuggler library for Icy
 * 
 * @author Stephane Dallongeville
 */
public class XugglerPlugin extends Plugin implements PluginLibrary
{
    static boolean initialized = false;

    /**
     * Force initialization (native library loading mainly) of Xuggler library
     */
    public static synchronized void init()
    {
        // we try the easier library loading process
        if (!initialized)
        {
            Throwable error = null;

            if (loadLibrary(XugglerPlugin.class, "xuggle"))
                initialized = true;

            try
            {

                Ferry.load();
                initialized = true;
            }
            catch (Throwable t)
            {
                error = t;
            }

            if (!initialized)
            {
                if (error != null)
                    System.err.println(error.getMessage());
                System.err.println("Xuggler native library load failed.");
            }
        }
    }

    /**
     * @return true if Xuggler library has been initialized
     */
    public static boolean isInitialized()
    {
        return initialized;
    }
}
